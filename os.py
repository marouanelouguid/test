from django.conf import settings
from django.core.mail import send_mail

send_mail(
    'Hello Python',
    'always coding and automation.',
    'marouanetester@gmail.com',
    ['marouane178@gmail.com'],
    fail_silently=False,
)